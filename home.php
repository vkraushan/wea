
<div id="animout-wrapper"
    data-animout-name="swirl-out-back"
    data-animout-duration=".9s"
    data-animout-easing="cubic-bezier(0.470, 0.000, 0.745, 0.715)"
    data-animout-fill-mode="both"
    data-animout-apply-to="#logo-wrapper">
    <div id="homepage-content">
        <div id="logo-wrapper"
            data-animlogo-name="swirl-in-front"
            data-animlogo-duration="1.1s"
            data-animlogo-easing="cubic-bezier(0.250, 0.460, 0.450, 0.940)"
            data-animlogo-fill-mode="both">
            <div id="words-wrapper">
                <div class="logo-word tex vertical done" data-xPos="108" data-yPos="272" data-fontSize="17">creativity</div>
                <div class="logo-word fg-demi-extra-comp all-caps done" data-xPos="108" data-yPos="104" data-fontSize="21">development</div>
                <div class="logo-word chopin capitalize done" data-xPos="5" data-yPos="181" data-fontSize="20">rewards</div>
                <div class="logo-word chopin capitalize vertical done" data-xPos="177" data-yPos="26" data-fontSize="18">performance</div>
                <div class="logo-word tex all-caps done" data-xPos="53" data-yPos="343" data-fontSize="20">team building</div>
                <div class="logo-word fg-demi-extra-comp all-caps vertical done" data-xPos="190" data-yPos="270" data-fontSize="29">Team work</div>
                <div class="logo-word fg-demi-extra-comp all-caps done" data-xPos="5" data-yPos="210"  data-fontSize="22">excellence</div>
                <div class="logo-word fg-demi-extra-comp vertical done" data-xPos="215" data-yPos="65" data-fontSize="28">success</div>
                <div class="logo-word  capitalize vertical done" data-xPos="235" data-yPos="261" data-fontSize="17">brand</div>
                <div class="logo-word chopin capitalize vertical done" data-xPos="310" data-yPos="267" data-fontSize="23">career</div>
                <div class="logo-word tex all-caps done" data-xPos="28" data-yPos="45" data-fontSize="20">responsibility</div>
                <div class="logo-word fg-book-extra-comp all-caps" data-xPos="159" data-yPos="295" data-fontSize="19">trainings</div>
                <div class="logo-word fg-book-extra-comp all-caps vertical done" data-xPos="289" data-yPos="64" data-fontSize="28">travel</div>
                <div class="logo-word fg-demi-extra-comp all-caps vertical done" data-xPos="53" data-yPos="212" data-fontSize="21">technology</div>
                <div class="logo-word fg-book-extra-comp all-caps done" data-xPos="272" data-yPos="268" data-fontSize="23">learning</div>
                <div class="logo-word tex all-caps vertical done" data-xPos="235" data-yPos="72" data-fontSize="10">leadership</div>
                <div class="logo-word fg-demi-extra-comp all-caps vertical done" data-xPos="154" data-yPos="25" data-fontSize="23">motivation</div>
                <div class="logo-word fg-book-extra-comp vertical" data-xPos="48" data-yPos="319" data-fontSize="22"></div>
                <div class="logo-word tex all-caps done" data-xPos="190" data-yPos="112" data-fontSize="13">work life balance</div>
                <div class="logo-word fg-demi-extra-comp all-caps done" data-xPos="158" data-yPos="246" data-fontSize="23">Execution</div>
                <div class="logo-word tex all-caps vertical done" data-xPos="26" data-yPos="104" data-fontSize="17">strategy</div>
                <div class="logo-word fg-demi-extra-comp all-caps done" data-xPos="53" data-yPos="261" data-fontSize="24">growth</div>
                <div class="logo-word fg-demi-extra-comp all-caps done" data-xPos="232" data-yPos="-1" data-fontSize="26">engagements</div>
                <div class="logo-word chopin capitalize" data-xPos="186" data-yPos="102" data-fontSize="23"></div>
                <div class="logo-word chopin capitalize done" data-xPos="50" data-yPos="317" data-fontSize="22">professional brand</div>
                <div class="logo-word tex all-caps vertical done" data-xPos="40" data-yPos="194" data-fontSize="18">collaboration</div>
                <div class="logo-word tex all-caps done" data-xPos="5" data-yPos="241" data-fontSize="13">Planning</div>
                <div class="logo-word tex vertical done" data-xPos="-35" data-yPos="86" data-fontSize="24">coaching</div>
                <div class="logo-word tex done" data-xPos="53" data-yPos="287" data-fontSize="23">offsites</div>
                <div class="logo-word fg-demi-extra-comp all-caps vertical" data-xPos="122" data-yPos="18" data-fontSize="20">culture</div>
                <div class="logo-word tex all-caps done" data-xPos="261" data-yPos="290" data-fontSize="13">voice of employee</div>
                <div class="logo-word fg-demi-extra-comp vertical done" data-xPos="52" data-yPos="100" data-fontSize="30">ownership</div>
                <div class="logo-word chopin capitalize vertical" data-xPos="68" data-yPos="125" data-fontSize="23"></div>
                <div class="logo-word fg-demi-extra-comp all-caps vertical" data-xPos="75" data-yPos="319" data-fontSize="22"></div>
                <div class="logo-word tex all-caps done" data-xPos="5" data-yPos="162" data-fontSize="13">productivity</div>
                <div class="logo-word tex vertical done" data-xPos="331" data-yPos="276" data-fontSize="17">Diversity</div>
                <div class="logo-word tex done" data-xPos="106" data-yPos="74" data-fontSize="22">networking</div>
                <div class="logo-word tex vertical done" data-xPos="147" data-yPos="15" data-fontSize="15">Ideas</div>
                <div class="logo-word tex " data-xPos="274" data-yPos="244" data-fontSize="16">stay in</div>
                <div class="logo-word tex done" data-xPos="230" data-yPos="28" data-fontSize="15">recognition</div>
                <div class="logo-word chopin capitalize vertical done" data-xPos="2" data-yPos="102" data-fontSize="22">feedback</div>
                <div class="logo-feedback fg-demi-extra-comp vertical" data-xPos="334" data-yPos="243" data-fontSize="25"></div>
                <div class="logo-word chopin vertical" data-xPos="197" data-yPos="296" data-fontSize="24"></div>
                <div class="logo-word chopin capitalize vertical" data-xPos="313" data-yPos="280" data-fontSize="26"></div>
                <div class="logo-word chopin capitalize vertical done" data-xPos="259" data-yPos="62" data-fontSize="26">skills</div>
                <div class="logo-word fg-book-extra-comp all-caps done" data-xPos="330" data-yPos="108" data-fontSize="19">career</div>
                <div class="logo-word tex all-caps done" data-xPos="158" data-yPos="274" data-fontSize="10">effeciency</div>
                <div class="logo-word  all-caps " data-xpos="58" data-ypos="92" style="left: 58px; top: 92px;"><img style="margin-left: 70px;margin-top: 20px;" src="images/final.png" width="70%"></div>
            </div>
            <!-- /#words-wrapper -->
            <nav id="nav-wrapper">
                <a href="website_html/what.html" id="logo-news" class="nav logo-word vertical" data-xPos="297" data-yPos="27" data-fontSize="56">What</a>
                <a href="website_html/contact.html" id="logo-contact" class="nav logo-word" data-xPos="215" data-yPos="326" data-fontSize="56">Contact</a>
                <a href="website_html/about.html" id="logo-about" class="nav logo-word" data-xPos="6" data-yPos="0" data-fontSize="56">About</a>
                <a href="website_html/work.html" id="logo-work" class="nav logo-word vertical" data-xPos="-32" data-yPos="290" data-fontSize="56">Work</a>
            </nav>
            <!-- /#nav-wrapper -->
        
        </div>
        <!-- /#logo-wrapper -->

    
    </div>
</div>